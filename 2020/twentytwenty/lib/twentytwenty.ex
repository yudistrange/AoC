defmodule Twentytwenty do
  @moduledoc """
  Documentation for `Twentytwenty`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Twentytwenty.hello()
      :world

  """
  def hello do
    :world
  end
end
