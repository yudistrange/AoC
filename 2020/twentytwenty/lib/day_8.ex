defmodule Twentytwenty.Day8 do
  @file_name "_priv/day_8.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split("\n")
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.map(fn x ->
      [instruction, value] = String.split(x)
      {val, _} = Integer.parse(value)
      {instruction, val, 0}
    end)
  end

  def increment_usage_count(instruction_list, index) do
    case Enum.at(instruction_list, index) do
      {instruction, value, usage_count} ->
        List.replace_at(instruction_list, index, {instruction, value, usage_count + 1})

      nil ->
        instruction_list
    end
  end

  def solver(instruction_list, index, acc, previous_index, pass_through \\ False) do
    {instruction, value, usage_count} = Enum.at(instruction_list, index)

    if usage_count != 0 do
      if pass_through == True do
        previous_index
      else
        acc
      end
    else
      case instruction do
        "acc" ->
          solver(
            increment_usage_count(instruction_list, index),
            index + 1,
            acc + value,
            index,
            pass_through
          )

        "nop" ->
          solver(
            increment_usage_count(instruction_list, index),
            index + 1,
            acc,
            index,
            pass_through
          )

        "jmp" ->
          solver(
            increment_usage_count(instruction_list, index),
            index + value,
            acc,
            index,
            pass_through
          )
      end
    end
  end

  def solve(pass_through \\ False) do
    parse_input()
    |> solver(0, 0, 0, pass_through)
  end

  def faulty_instruction_index(instruction_list, index, acc, index_list, skip_index) do
    updated_instruction_list = increment_usage_count(instruction_list, index)

    case Enum.at(instruction_list, index) do
      {instruction, value, usage_count} ->
        case instruction do
          "acc" ->
            if usage_count == 0 do
              faulty_instruction_index(
                updated_instruction_list,
                index + 1,
                acc + value,
                [{"acc", index, acc + value} | index_list],
                skip_index
              )
            else
              [{"acc", index, acc} | index_list]
            end

          "nop" ->
            if usage_count == 0 do
              if index != skip_index do
                faulty_instruction_index(
                  updated_instruction_list,
                  index + 1,
                  acc,
                  [{"nop", index, acc} | index_list],
                  skip_index
                )
              else
                faulty_instruction_index(
                  updated_instruction_list,
                  index + value,
                  acc,
                  [{"nop->jmp", index, acc} | index_list],
                  skip_index
                )
              end
            else
              [{"nop", index, acc} | index_list]
            end

          "jmp" ->
            if usage_count == 0 do
              if index != skip_index do
                faulty_instruction_index(
                  updated_instruction_list,
                  index + value,
                  acc,
                  [{"jmp", index, acc} | index_list],
                  skip_index
                )
              else
                faulty_instruction_index(
                  updated_instruction_list,
                  index + 1,
                  acc,
                  [{"jmp->nop", index, acc} | index_list],
                  skip_index
                )
              end
            else
              [{"jmp", index, acc} | index_list]
            end
        end

      nil ->
        index_list
    end
  end

  def solve_2nd do
    instruction_set = parse_input()

    Enum.map(0..637, fn skip_index ->
      faulty_instruction_index(instruction_set, 0, 0, [], skip_index) |> List.first()
    end)
    |> MapSet.new()
    |> Enum.sort(fn {_, i, _}, {_, i1, _} ->
      i >= i1
    end)
  end
end
