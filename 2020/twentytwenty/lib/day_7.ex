defmodule Twentytwenty.Day7 do
  @file_name "_priv/day_7.input"

  def parse_input() do
    @file_name
    |> File.read!()
    |> String.split("\n")
  end

  def unique_parents(map, set, key) do
    parents = Map.get(map, key, [])
    parents_set = MapSet.union(set, MapSet.new(parents))

    Enum.reduce(parents, parents_set, fn p, s ->
      unique_parents(map, s, p)
    end)
  end

  def solve do
    parse_input()
    |> Enum.map(fn rule ->
      rule
      |> String.replace(["contain", ",", "."], " ")
      |> String.replace("bags", "bag")
      |> String.replace(~r/[[:digit:]]/, " ")
      |> String.split("  ")
      |> Enum.filter(fn x -> x != "" end)
      |> Enum.map(fn x -> String.trim(x) end)
    end)
    |> Enum.reduce(Map.new(), fn rule, acc ->
      {parent, rest} = List.pop_at(rule, 0)

      Enum.reduce(rest, acc, fn r, a ->
        Map.update(a, r, [parent], fn op -> [parent | op] end)
      end)
    end)
  end

  def total_bags(map, key) do
    bags = Map.get(map, key)

    count =
      Enum.reduce(bags, 0, fn {count, bag_key}, acc ->
        if count == 0 do
          1
        else
          acc + count * total_bags(map, bag_key)
        end
      end)

    IO.puts("#{key}, #{count}")
    count
  end

  def bag_count(map, key) do
    bag_names = Map.get(map, key)

    Enum.map(bag_names, fn bag ->
      case bag do
        {count, nil} ->
          count

        {count, bag_name} ->
          total = 1 + count * bag_count(map, bag_name)
          IO.inspect("#{bag_name}: #{total}")
          total
      end
    end)
    |> Enum.sum()
  end

  def solve_2nd do
    parse_input()
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.map(fn rule ->
      rule
      |> String.split([" contain", ",", "."])
      |> Enum.filter(fn x -> x != "" end)
      |> Enum.map(fn x ->
        x
        |> String.replace("bags", "bag")
        |> String.trim()
      end)
    end)
    |> Enum.reduce(Map.new(), fn [key | container_bags], acc ->
      Map.put(
        acc,
        key,
        Enum.map(container_bags, fn val ->
          case val do
            "no other bag" ->
              {0, nil}

            x ->
              [count | bags] = String.split(x)
              {c, _} = Integer.parse(count)
              {c, Enum.join(bags, " ")}
          end
        end)
      )
    end)
  end
end
