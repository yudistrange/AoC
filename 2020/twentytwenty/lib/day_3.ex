defmodule Twentytwenty.Day3 do
  @file_name "_priv/day_3.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split("\n")
    |> Enum.filter(fn x -> x != "" end)
  end

  def solve(right_jump, down_jump) do
    input = parse_input()
    input_len = String.length(Enum.at(input, 0))

    input
    |> Enum.reduce({right_jump, 0, 0}, fn input_line, {x_pos, y_pos, count} ->
      if rem(y_pos, down_jump) == 0 and y_pos != 0 do
        case String.at(input_line, x_pos) do
          "#" -> {rem(x_pos + right_jump, input_len), y_pos + 1, count + 1}
          _ -> {rem(x_pos + right_jump, input_len), y_pos + 1, count}
        end
      else
        {x_pos, y_pos + 1, count}
      end
    end)
  end
end
