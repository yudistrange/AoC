defmodule Twentytwenty.Day5 do
  @file_name "_priv/day_5.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split("\n")
    |> Enum.filter(fn x -> x != "" end)
  end

  def parse_code(bottom, top, code) do
    result =
      code
      |> String.split("")
      |> Enum.reduce("", fn c, acc ->
        cond do
          c == top -> acc <> "1"
          c == bottom -> acc <> "0"
          true -> acc
        end
      end)

    Integer.parse(result, 2)
  end

  def solve do
    parse_input()
    |> Enum.map(fn code ->
      {row, _} = parse_code("F", "B", code)
      {col, _} = parse_code("L", "R", code)

      row * 8 + col
    end)
  end
end
