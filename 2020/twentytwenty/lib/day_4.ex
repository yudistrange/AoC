defmodule Twentytwenty.Day4 do
  @file_name "_priv/day_4.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split("\n\n")
    |> Enum.map(fn x ->
      String.split(x)
    end)
  end

  def solve do
    passport_rules = MapSet.new(["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"])

    parse_input()
    |> Enum.map(fn passport ->
      current_passport_keys =
        Enum.reduce(passport, MapSet.new(), fn str, acc ->
          key = String.split(str, ":") |> List.first()
          MapSet.put(acc, key)
        end)

      MapSet.difference(passport_rules, current_passport_keys) |> Enum.count() == 0
    end)
  end
end
