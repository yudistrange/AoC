defmodule Twentytwenty.Day10 do
  @file_name "_priv/day_10.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split()
    |> Enum.map(fn str ->
      {num, ""} = Integer.parse(str)
      num
    end)
    |> Enum.sort()
  end

  def solve do
    adaptors = parse_input()
    device_adaptor = List.last(adaptors) + 3

    (adaptors ++ [device_adaptor])
    |> Enum.reduce({Map.new(), 0}, fn current_joltage, {map, prev_joltage} ->
      diff = current_joltage - prev_joltage
      {Map.update(map, diff, 1, fn x -> x + 1 end), current_joltage}
    end)
  end

  def recursive_tree(lst) do
    current_joltage = Enum.at(lst, 0)
    next_joltage = Enum.take(Enum.drop(lst, 1), 3)

    cond do
      Enum.count(lst) == 2 ->
        :ets.insert(:joltages, {current_joltage, 1})
        1

      :ets.lookup(:joltages, current_joltage) != [] ->
        [{_key, sum}] = :ets.lookup(:joltages, current_joltage)
        sum

      current_joltage + 3 >= Enum.at(next_joltage, 2) ->
        sum =
          recursive_tree(Enum.drop(lst, 1)) + recursive_tree(Enum.drop(lst, 2)) +
            recursive_tree(Enum.drop(lst, 3))

        :ets.insert(:joltages, {current_joltage, sum})
        sum

      current_joltage + 3 >= Enum.at(next_joltage, 1) ->
        sum = recursive_tree(Enum.drop(lst, 1)) + recursive_tree(Enum.drop(lst, 2))
        :ets.insert(:joltages, {current_joltage, sum})
        sum

      current_joltage + 3 >= Enum.at(next_joltage, 0) ->
        sum = recursive_tree(Enum.drop(lst, 1))
        :ets.insert(:joltages, {current_joltage, sum})
        sum
    end
  end

  def solve_2nd() do
    :ets.new(:joltages, [:set, :named_table])
    adaptors = parse_input()
    device_adaptor = List.last(adaptors) + 3
    total = adaptors ++ [device_adaptor]

    recursive_tree(total)

    list = :ets.tab2list(:joltages) |> Enum.sort_by(fn {k1, _} -> k1 end)
    :ets.delete(:joltages)
    list
  end
end
