defmodule Twentytwenty.Day9 do
  @file_name "_priv/day_9.input"
  @preamble 25

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split()
    |> Enum.map(fn x ->
      {num, _} = Integer.parse(x)
      num
    end)
  end

  def codex_contains_sum?(codex, check_val) do
    codex
    |> Enum.map(fn code ->
      MapSet.member?(codex, check_val - code)
    end)
    |> Enum.reduce(false, fn x, a -> x or a end)
  end

  def first_invalid_number(codex, input, preamble \\ @preamble) do
    case Enum.at(input, preamble) do
      nil ->
        "exiting"

      check_val ->
        valid? = codex_contains_sum?(codex, check_val)

        case valid? do
          true ->
            new_input = Enum.drop(input, 1)

            first_invalid_number(
              new_input |> Enum.take(preamble) |> MapSet.new(),
              new_input,
              preamble
            )

          false ->
            IO.inspect(codex)
            IO.inspect(input)
            check_val
        end
    end
  end

  def solve(preamble \\ @preamble) do
    input = parse_input()
    codex = Enum.take(input, preamble) |> MapSet.new()
    first_invalid_number(codex, input, preamble)
  end

  def drop_elements_till_sum_is_less(lst, check_sum) do
    cond do
      Enum.sum(lst) > check_sum ->
        drop_elements_till_sum_is_less(Enum.drop(lst, 1), check_sum)

      Enum.sum(lst) < check_sum ->
        {:cont, lst}

      Enum.sum(lst) == check_sum ->
        {:halt, lst}
    end
  end

  def solve_2nd(check_val) do
    input = parse_input()

    list_of_nums =
      Enum.reduce_while(input, [], fn v, acc ->
        sum_thus_far = Enum.sum(acc)

        cond do
          sum_thus_far + v < check_val ->
            {:cont, acc ++ [v]}

          sum_thus_far + v > check_val ->
            drop_elements_till_sum_is_less(acc ++ [v], check_val)

          sum_thus_far + v == check_val ->
            {:halt, acc}
        end
      end)

    sorted_nums = Enum.sort(list_of_nums)
    Enum.at(sorted_nums, 0) + Enum.at(sorted_nums, Enum.count(sorted_nums) - 1)
  end
end
