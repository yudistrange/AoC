defmodule Twentytwenty.Day6 do
  @file_name "_priv/day_6.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split("\n\n")
  end

  def solve do
    parse_input()
    |> Enum.map(fn responses ->
      String.split(responses, "")
      |> Enum.filter(fn x -> x != "\n" and x != "" end)
      |> MapSet.new()
      |> Enum.count()
    end)
    |> Enum.sum()
  end

  def solve_2nd do
    parse_input()
    |> Enum.map(fn responses ->
      String.split(responses, "\n")
      |> Enum.filter(fn x -> x != "" end)
      |> IO.inspect()
      |> Enum.map(fn x ->
        String.split(x, "") |> Enum.filter(fn x -> x != "\n" and x != "" end) |> MapSet.new()
      end)
      |> IO.inspect()
      |> Enum.reduce(fn elem, acc ->
        MapSet.intersection(elem, acc)
      end)
      |> Enum.count()
    end)
    |> Enum.sum()
  end
end
