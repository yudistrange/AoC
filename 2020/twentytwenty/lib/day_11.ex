defmodule Twentytwenty.Day11 do
  @file_name "_priv/day_11.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split()
  end
end
