defmodule Twentytwenty.Day2 do
  @file_name "_priv/day_2.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split("\n")
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.map(fn str ->
      [count_rule, character_rule, password] = String.split(str, " ")
      [min_count, max_count] = String.split(count_rule, "-")
      [character, _] = String.split(character_rule, ":")
      {String.to_integer(min_count), String.to_integer(max_count), character, password}
    end)
  end

  def inc(x) do
    x + 1
  end

  def character_map(str) do
    str
    |> String.split("")
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.reduce(Map.new(), fn x, acc ->
      Map.update(acc, x, 1, &inc/1)
    end)
  end

  def solve do
    parse_input()
    |> Enum.map(fn {min, max, chr, password} ->
      cm = character_map(password)
      min <= Map.get(cm, chr) and Map.get(cm, chr) <= max
    end)
  end

  def solve_2nd do
    parse_input()
    |> Enum.map(fn {min_loc, max_loc, chr, password} ->
      (String.at(password, min_loc - 1) == chr and String.at(password, max_loc - 1) != chr) or
        (String.at(password, min_loc - 1) != chr and String.at(password, max_loc - 1) == chr)
    end)
  end
end
