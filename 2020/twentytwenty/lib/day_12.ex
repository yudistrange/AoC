defmodule Twentytwenty.Day12 do
  @file_name "_priv/day_12.input"

  @initial_position {"E", 0, 0}

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split()
    |> Enum.map(fn line ->
      {action, scale} = String.split_at(line, 1)
      {scale_int, _} = Integer.parse(scale)
      IO.inspect({action, scale_int})
    end)
  end

  def turn_left(direction) do
    case direction do
      "N" -> "W"
      "W" -> "S"
      "S" -> "E"
      "E" -> "N"
    end
  end

  def turn_right(direction) do
    case direction do
      "N" -> "E"
      "W" -> "N"
      "S" -> "W"
      "E" -> "S"
    end
  end

  def turn(heading, turn_direction, degrees) do
    num_of_turns = div(degrees, 90)

    case turn_direction do
      "L" ->
        Enum.reduce(1..num_of_turns, heading, fn _, current_direction ->
          turn_left(current_direction)
        end)

      "R" ->
        Enum.reduce(1..num_of_turns, heading, fn _, current_direction ->
          turn_right(current_direction)
        end)
    end
  end

  def move({heading, x, y}, direction, scale) do
    case direction do
      "N" -> {heading, x + scale, y}
      "S" -> {heading, x - scale, y}
      "E" -> {heading, x, y - scale}
      "W" -> {heading, x, y + scale}
    end
  end

  def solve(initial_pos \\ @initial_position) do
    final_coords =
      Enum.reduce(parse_input(), initial_pos, fn {intent, scale}, {heading, x, y} ->
        case intent do
          "N" -> move({heading, x, y}, "N", scale)
          "E" -> move({heading, x, y}, "E", scale)
          "S" -> move({heading, x, y}, "S", scale)
          "W" -> move({heading, x, y}, "W", scale)
          "F" -> move({heading, x, y}, heading, scale)
          "L" -> {turn(heading, "L", scale), x, y}
          "R" -> {turn(heading, "R", scale), x, y}
        end
      end)

    {_, x, y} = final_coords
    abs(x) + abs(y)
  end
end
