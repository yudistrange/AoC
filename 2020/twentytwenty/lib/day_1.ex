defmodule Twentytwenty.Day1 do
  @file_name "_priv/day_1.input"

  def parse_input do
    @file_name
    |> File.read!()
    |> String.split("\n")
    |> Enum.filter(fn x -> x != "" end)
    |> Enum.map(&String.to_integer/1)
    |> MapSet.new()
  end

  def solve do
    num_set = parse_input()
    Enum.find(num_set, fn x -> MapSet.member?(num_set, 2020 - x) end)
  end

  def solve_2nd do
    num_set = parse_input()

    num_set
    |> Enum.map(fn num ->
      found = Enum.find(num_set, fn x -> MapSet.member?(num_set, 2020 - num - x) end)

      if found == nil do
        nil
      else
        {num, found, 2020 - num - found}
      end
    end)
  end
end
