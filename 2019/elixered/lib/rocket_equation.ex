defmodule Elixered.RocketEquation do
  def fuel_for_module(m) when m < 6, do: 0
  def fuel_for_module(m), do: div(m, 3) - 2

  def fuel_for_modules(series) do
    series
    |> Enum.map(fn x -> fuel_for_module(x) end)
    |> Enum.sum()
  end

  def fuel_for_everything(fuel) do
    f_for_f = fuel_for_module(fuel)

    cond do
      f_for_f > 0 ->
        f_for_f + fuel_for_everything(f_for_f)

      true ->
        f_for_f
    end
  end
end
