defmodule Elixered.ProgramAlarm do
  require IEx

  def intcode(ops, index \\ 0) do
    case Enum.at(ops, index) do
      1 -> intcode_processor(:add, ops, index)
      2 -> intcode_processor(:mult, ops, index)
      99 -> intcode_processor(:halt, ops, index)
    end
  end

  defp intcode_processor(:add, ops, index) do
    arg1 = Enum.at(ops, Enum.at(ops, index + 1))
    arg2 = Enum.at(ops, Enum.at(ops, index + 2))
    result = arg1 + arg2

    intcode(ops |> List.replace_at(Enum.at(ops, index + 3), result), index + 4)
  end

  defp intcode_processor(:mult, ops, index) do
    arg1 = Enum.at(ops, Enum.at(ops, index + 1))
    arg2 = Enum.at(ops, Enum.at(ops, index + 2))
    result = arg1 * arg2

    intcode(ops |> List.replace_at(Enum.at(ops, index + 3), result), index + 4)
  end

  defp intcode_processor(:halt, ops, index) do
    ops
  end

  def brute_force(ops) do
    for i <- 0..99 do
      for j <- 0..99 do
        n_ops = ops |> List.replace_at(1, i) |> List.replace_at(2, j)
        result = intcode(n_ops)

        if Enum.at(result, 0) == 19_690_720 do
          {i, j}
        else
          nil
        end
      end
    end
  end
end
