defmodule Elixered.CrossedWires do
  def path_splitter(path_string), do: String.split(path_string, ",")

  defp tokenize(path) do
    {dir, len} = String.split_at(path, 1)
    {dir, String.to_integer(len)}
  end

  def trace(path) do
    path
    |> path_splitter()
    |> Enum.map(fn p -> tokenize(p) end)
    |> Enum.reduce({[], {0, 0}}, fn {dir, len}, {point_set, origin} ->
      path_fragments(point_set, {dir, len}, origin)
    end)
  end

  defp path_fragments(point_set, {dir, len}, {_x, _y} = start_point) do
    case dir do
      "R" -> path_tracer(point_set, start_point, len, 0, 1)
      "L" -> path_tracer(point_set, start_point, len, 0, -1)
      "D" -> path_tracer(point_set, start_point, 0, len, -1)
      "U" -> path_tracer(point_set, start_point, 0, len, 1)
    end
  end

  defp path_tracer(point_set, origin, 0, 0, _flag), do: {point_set, origin}

  defp path_tracer(point_set, {start_x, start_y}, 0, y_len, flag) when y_len > 0 do
    new_origin = {start_x, start_y + flag}
    path_tracer([new_origin | point_set], new_origin, 0, y_len - 1, flag)
  end

  defp path_tracer(point_set, {start_x, start_y}, x_len, 0, flag) when x_len > 0 do
    new_origin = {start_x + flag, start_y}
    path_tracer([new_origin | point_set], new_origin, x_len - 1, 0, flag)
  end

  defp path_tracer(point_set, origin, _x_len, _y_len, _flag) do
    IO.puts("Error")
    {point_set, origin}
  end

  def part_1(wire_1, wire_2) do
    {path_set_1, _} = trace(wire_1)
    {path_set_2, _} = trace(wire_2)

    MapSet.intersection(MapSet.new(path_set_1), MapSet.new(path_set_2))
    |> IO.inspect()
    |> Enum.map(fn {x, y} -> abs(x) + abs(y) end)
    |> Enum.sort()
    |> List.first()
  end

  def part_2(wire_1, wire_2) do
    {path_set_1, _} = trace(wire_1)
    {path_set_2, _} = trace(wire_2)

    ps1 = Enum.reverse(path_set_1) |> IO.inspect()
    ps2 = Enum.reverse(path_set_2) |> IO.inspect()

    intersections = MapSet.intersection(MapSet.new(path_set_1), MapSet.new(path_set_2))

    intersections
    |> IO.inspect()
    |> Enum.map(fn intersect ->
      wire_1_len = Enum.find_index(path_set_1, fn point -> point == intersect end)
      wire_2_len = Enum.find_index(path_set_2, fn point -> point == intersect end)
      wire_1_len + wire_2_len
    end)
    |> Enum.sort()
  end
end
