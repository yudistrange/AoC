defmodule Elixered.SecuredContainer do
  def pass_codes(starting_num, ending_num) do
    starting_num..ending_num
    |> Enum.map(fn n -> tokenize(n) end)
    |> Enum.filter(fn n -> adjacent_digits_same?(n) end)
    |> Enum.filter(fn n -> increasing_digits?(n) end)
    |> Enum.filter(fn n ->
      if !pairwise_adjacent_same_digits?(n) do
        IO.puts(n)
      end

      pairwise_adjacent_same_digits?(n)
    end)
    |> Enum.count()
  end

  defp tokenize(pass_code) do
    pass_code
    |> Integer.to_string()
    |> String.to_charlist()
  end

  defp adjacent_digits_same?(pass_code_string) do
    {_, result} =
      Enum.reduce(pass_code_string, {nil, false}, fn c, {previous_char, result} ->
        if c == previous_char do
          {c, true}
        else
          {c, result}
        end
      end)

    result
  end

  def increasing_digits?(pass_code_string) do
    {_, result} =
      Enum.reduce(pass_code_string, {0, true}, fn c, {previous_char, result} ->
        if c >= previous_char do
          {c, result}
        else
          {c, false}
        end
      end)

    result
  end

  def pairwise_adjacent_same_digits?(pass_code_string) do
    pass_code_string
    |> Enum.reduce(Map.new(), fn c, m ->
      Map.update(m, c, 1, fn x -> x + 1 end)
    end)
    |> Map.values()
    |> MapSet.new()
    |> MapSet.member?(2)
  end
end
