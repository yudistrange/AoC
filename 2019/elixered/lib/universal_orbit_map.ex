defmodule Elixered.UniversalOrbitMap do
  defmodule Storage do
    use GenServer

    def start(), do: GenServer.start_link(__MODULE__, [], name: __MODULE__)
    def init(_), do: {:ok, %{}}
    def store({center, orbiter}), do: GenServer.cast(__MODULE__, {:store, center, orbiter})
    def total_orbit_length(), do: GenServer.call(__MODULE__, {:total_orbit_length})

    def handle_cast({:store, "COM", orbiter}, state) do
      {:noreply, state |> Map.put(orbiter, 1)}
    end

    def handle_cast({:store, center, orbiter}, state) do
      case Map.get(state, orbiter) do
        nil ->
          case Map.get(state, center) do
            nil ->
              Task.start(fn ->
                :timer.sleep(100)
                store({center, orbiter})
              end)
              {:noreply, state}
            center_orbit_length ->
              {:noreply, state |> Map.put(orbiter, center_orbit_length + 1)}
          end
        _ ->
          IO.puts ("Duplicate update for #{orbiter}")
          {:noreply, state}
      end
    end

    def handle_call({:total_orbit_length}, _from, state) do
      total = state |> Map.values() |> Enum.sum()
      {:reply, total, state}
    end
  end

  def solve_1(orbit_map) do
    Storage.start()

    orbit_map
    |> tokenize()
    |> store_orbital_distances()

    total_orbit_distance()
  end

  def solve_2(orbit_map) do
    tokenized_orbit_map = orbit_map |> tokenize()
    santas_center = Map.get(tokenized_orbit_map, "SAN")
    your_center = Map.get(tokenized_orbit_map, "YOU")

    Enum.reduce_while(tokenized_orbit_map, fn {orbiter, center} ->
    end)
  end

  def orbiter_to_center_map(orbit_map) do
    orbit_map
    |> Enum.reduce(%{}, fn {center, vals}, acc ->
      Enum.reduce(vals, acc, fn v, a ->
        Map.put(a, v, center)
      end)
    end)
  end

  def tokenize(orbit_map) do
    orbit_map
    |> String.split("\n")
    |> Enum.reduce(%{}, fn x, acc_map ->
      [k, v] = String.split(x, ")")
      Map.update(acc_map, k, [v], fn val -> [v | val] end)
    end)
  end

  def store_orbital_distances(orbit_map) do
    lowest_orbit = Map.get(orbit_map, "COM")
    Enum.map(lowest_orbit, fn orbiter ->
      parse_orbits("COM", orbiter)
    end)

    Enum.map(orbit_map, fn {key, vals} ->
      Enum.map(vals, fn val ->
        parse_orbits(key, val)
      end)
    end)
  end

  def total_orbit_distance() do
    Storage.total_orbit_length()
  end

  defp parse_orbits(center, orbiter) do
    Storage.store({center, orbiter})
  end
end
