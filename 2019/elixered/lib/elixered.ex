defmodule Elixered do
  @moduledoc """
  Documentation for Elixered.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Elixered.hello()
      :world

  """
  def hello do
    :world
  end
end
